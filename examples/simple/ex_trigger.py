"""
   pyfly2
   
   example_opencv_integration

   continually grab images from a camera and display them using OpenCV's
   HighGui module, until the user presses a key
"""

import cv2
import pyfly2


def main(cameraIndex=2, format='bgr', scale=1.0, windowName='Live Video'):
    context = pyfly2.Context()
    if context.num_cameras < 1:
        raise ValueError('No cameras found')
    camera = context.get_camera(cameraIndex)
    camera.Connect()
    trig = camera.GetTriggerMode()
    trig["onOff"] = True
    camera.SetTriggerMode(trig)
    camera.StartCapture()
    camera.FireSoftwareTrigger()
    image = camera.GrabNumPyImage('bgr')
    camera.StopCapture()
    cv2.imwrite("test.png",image)
if __name__ == "__main__":
    main()
